from rest_framework import serializers
from polls.models import *


class ProvinceSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=40)
    id = serializers.IntegerField(default=0)
    lowest_con = serializers.IntegerField(default=0)
    highest_con = serializers.IntegerField(default=0)


class ScoreSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=40)
    con_id = serializers.IntegerField(default=0)
    votes = serializers.IntegerField(default=0)


class ConstituencySerializer(serializers.Serializer):
    name = serializers.CharField(max_length=40)
    id = serializers.IntegerField(default=0)
    allowed = serializers.IntegerField(default=0)
    ballots = serializers.IntegerField(default=0)
    spoiled = serializers.IntegerField(default=0)