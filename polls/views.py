from polls.models import *
from polls.serializers import *
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from django.db.models import Q
from django.contrib.auth.models import User

# Create your views here.


class Province:
    def __init__(self, name, id, lowest_con, highest_con):
        self.name = name
        self.id = id
        self.lowest_con = lowest_con
        self.highest_con = highest_con


def custom_serialize(objects_list, custom_serializer):
    serializer = custom_serializer(objects_list, many=True)
    content = JSONRenderer().render(serializer.data)
    response = HttpResponse(content, content_type="application/json")
    response["Access-Control-Allow-Origin"] = "*"
    return response


def voivodeships(request):
    voivodeships = Voivodeship.objects.all()
    voi_list = []
    for voi in voivodeships:
        voi_obj = Province(voi.name, voi.id, voi.lowest_cons_number, voi.highest_cons_number)

        voi_list.append(voi_obj)

    return custom_serialize(voi_list, ProvinceSerializer)


def counties(request):
    counties = County.objects.all()
    cou_list = []
    for cou in counties:
        cou_obj = Province(cou.number, cou.id, cou.lowest_cons_number, cou.highest_cons_number)

        cou_list.append(cou_obj)

    return custom_serialize(cou_list, ProvinceSerializer)


class ConSend:
    def __init__(self, name, id, allowed, ballots, spoiled):
        self.name = name
        self.id = id
        self.allowed = allowed
        self.ballots = ballots
        self.spoiled = spoiled


def constituencies(request):
    constituencies = Constituency.objects.all()
    con_list = []
    for con in constituencies:
        con_obj = ConSend(con.name, con.id, con.allowed, con.ballots, con.spoiled_votes)

        con_list.append(con_obj)

    return custom_serialize(con_list, ConstituencySerializer)


class ScoreSend:
    def __init__(self, name, con_id, votes):
        self.name = name
        self.con_id = con_id
        self.votes = votes


def scores(request):
    scores = Score.objects.all()
    score_list = []
    for score in scores:
        score_obj = ScoreSend(score.name, score.constituency.id, score.votes_count)

        score_list.append(score_obj)

    return custom_serialize(score_list, ScoreSerializer)


def authenticate(request, login, password):
    users = User.objects.filter(username=login)
    if users.count() != 1:
        return HttpResponse("0")

    user = users[0]
    if user.check_password(password):
        return HttpResponse("1")

    return HttpResponse("0")


def register(request, login, password):
    users = User.objects.filter(username=login)
    if users.count() > 0:
        return HttpResponse("0")

    user = User.objects.create_user(username=login, password=password)
    user.is_active = True
    user.save()

    return HttpResponse("1")


def change(request, region, position, value):
    cons = Constituency.objects.all()
    con = cons[int(region)]
    scores = Score.objects.filter(constituency=con)
    scores = sorted(scores, key=lambda score: score.votes_count, reverse=True)
    score = scores[int(position) - 1]
    difference = int(value) - score.votes_count
    con.allowed += difference
    con.votes += difference
    con.ballots += difference
    con.save()
    score.votes_count = value
    score.save()

    return HttpResponse("1")
