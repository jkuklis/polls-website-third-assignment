from django.db import models


class Voivodeship(models.Model):
    name = models.CharField(max_length=40)
    lowest_cons_number = models.IntegerField(default=0)
    highest_cons_number = models.IntegerField(default=0)

    def __str__(self):
        return self.name \
               + " " \
               + str(self.lowest_cons_number) \
               + " " \
               + str(self.highest_cons_number)


class County(models.Model):
    number = models.IntegerField(default=0)
    lowest_cons_number = models.IntegerField(default=0)
    highest_cons_number = models.IntegerField(default=0)

    def __str__(self):
        return self.name \
               + " " \
               + str(self.lowest_cons_number) \
               + " " \
               + str(self.highest_cons_number)


class District(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class Constituency(models.Model):
    number = models.IntegerField(default=0)
    name = models.CharField(max_length=40)
    allowed = models.IntegerField()
    ballots = models.IntegerField()
    votes = models.IntegerField()
    spoiled_votes = models.IntegerField()
    district = models.ForeignKey(District)

    def __str__(self):
        return str(self.number) \
               + " " \
               + self.name


class Score(models.Model):
    name = models.CharField(max_length=40)
    constituency = models.ForeignKey(Constituency)
    votes_count = models.IntegerField(default=0)

    def __str__(self):
        return self.name \
               + " " \
               + str(self.constituency.name) +  " " + str(self.constituency.id) \
               + " " \
               + str(self.votes_count)
