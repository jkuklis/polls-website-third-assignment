
function auth_request(login, password) {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/authenticate/" + login + "/" + password);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {

        if (this.responseText === "1") {
            alert("Successfully logged in (reload page if login not displayed)");

            localStorage.setItem("login", login);
            localStorage.setItem("password", password);

        } else {
            alert("Failed to login");

            localStorage.removeItem("login");
            localStorage.removeItem("password");
        }
    });

    req.send();
}


function register_request(login, password) {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/register/" + login + "/" + password);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {
        
        if (this.responseText === "1") {
            alert("Successfully registered");

        } else {
            alert("Failed to register");
        }
    });

    req.send();
}


function login_display() {
    var div = document.getElementById("login");

    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }

    var login = localStorage.getItem("login");
    if (login !== null) {

        var welcome_div = div.appendChild(document.createElement("div"));
        welcome_div.appendChild(document.createTextNode("Cześć, " + login + "!"));

        var logout_div = div.appendChild(document.createElement("div"));
        var a = logout_div.appendChild(document.createElement("a"));
        a.appendChild(document.createTextNode("Wyloguj"));
        a.onclick = function() {
            localStorage.removeItem("login");
            localStorage.removeItem("password");

            if (localStorage.getItem("appended") === "yes") {
                localStorage.setItem("appended", "no");
                var header = document.getElementById("candidates_table_header");
                header.removeChild(header.lastChild);

                var cand_count = 12;

                for (var i = 1; i <= cand_count; i++) {
                    var td = document.getElementById("cand_change_" + i);
                    td.parentNode.removeChild(td);
                }

            }

            login_display();
        }

    } else {
        var login_div = div.appendChild(document.createElement("div"));

        var login_input = login_div.appendChild(document.createElement("input"));
        login_input.type = "text";login_input.placeholder = "Login";

        var password_input = login_div.appendChild(document.createElement("input"));
        password_input.type = "password";
        password_input.placeholder = "Hasło";

        var submit = login_div.appendChild(document.createElement("button"));
        submit.appendChild(document.createTextNode("Login"));
        submit.onclick = function() {
            auth_request(login_input.value, password_input.value);

            login_display();
        };

        var register_div = div.appendChild(document.createElement("div"));

        var register_login = register_div.appendChild(document.createElement("input"));
        register_login.type = "text";
        register_login.placeholder = "Login";

        var register_password = register_div.appendChild(document.createElement("input"));
        register_password.type = "password";
        register_password.placeholder = "Hasło";

        var register = register_div.appendChild(document.createElement("button"));
        register.appendChild(document.createTextNode("Rejestracja"));
        register.onclick = function() {
            register_request(register_login.value, register_password.value);
        }
    }
}


function region_display(region, region_number) {
    var prov_str;
    var cand_str;

    switch(region) {
        case "pol":
            prov_str = "województwach";
            cand_str = "kraju";
            break;
        case "voi":
            prov_str = "okręgach";
            cand_str = "województwie";
            break;
        case "cou":
            prov_str = "gminach";
            cand_str = "okręgu";
            break;
        case "con":
            prov_str = "gminie";
            cand_str = "gminy";
            break;
    }

    document.getElementById("provinces_header").appendChild(document.createTextNode(prov_str));
    document.getElementById("candidates_header").appendChild(document.createTextNode(cand_str));
}


function change_request(region_number, index, value) {
    var login = localStorage.getItem("login");
    var password = localStorage.getItem("password");
    if (login === null || password === null)
        return;
    var req_auth = new XMLHttpRequest();
    var logged = 1;
    req_auth.open("GET", "http://localhost:8000/authenticate/" + login + "/" + password);
    req_auth.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req_auth.addEventListener("load", function () {

        if (this.responseText !== "1") {
            logged = 0;
        }
    });

    if (logged === 0)
        return;

    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/change/" + region_number + "/" + index + "/" + value);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {

        if (this.responseText === "1") {

            var elt = document.getElementById("cand_" + index);
            var difference = value - parseInt(elt.placeholder);

            var scores = JSON.parse(localStorage.getItem("scores"));
            var candidates = JSON.parse(localStorage.getItem("candidates"));
            var constituencies = JSON.parse(localStorage.getItem("constituencies"));

            var name = candidates[parseInt(index) - 1]["name"];
            var begin = 12 * parseInt(region_number);
            for (var i = 0; i < 12; i++) {
                if (scores[begin + i]["name"] === name) {
                    scores[begin + i]["votes"] = parseInt(value);
                    break;
                }
            }

            var con = constituencies[region_number];

            con["allowed"] = parseInt(con["allowed"]) + difference;
            con["legit"] = parseInt(con["legit"]) + difference;
            con["attendance"] = (con["allowed"] === 0 ? 0 : con["legit"] / con["allowed"]);

            constituencies[region_number] = con;

            localStorage.setItem("constituencies", JSON.stringify(constituencies));
            localStorage.setItem("scores", JSON.stringify(scores));
            localStorage.setItem("candidates", JSON.stringify("candidates"));

            alert("Successfully changed, please reload page to see changes");

        } else {
            alert("Failed to change");
        }
    });

    req.send();
}


function provinces_display(region, region_number) {
    var data = JSON.parse(localStorage.getItem("provinces"));

    var provinces = document.getElementById("provinces");

    while (provinces.childElementCount > 1) {
        provinces.removeChild(provinces.lastChild);
    }


    for (var pro in data) {
        var tr = provinces.appendChild(document.createElement("tr"));

        var name = tr.appendChild(document.createElement("td"));
        var link = name.appendChild(document.createElement("a"));
        link.appendChild(document.createTextNode(data[pro]["name"]));

        link.href = window.location.pathname;

        switch(region) {
            case "pol":
                link.href = "/voi" + data[pro]["id"];
                break;
            case "voi":
                link.href = "/cou" + data[pro]["id"];
                break;
            case "cou":
                link.href = "/con" + data[pro]["id"];
                break;
        }

        var legit = tr.appendChild(document.createElement("td"));
        legit.appendChild(document.createTextNode(data[pro]["legit"]));

        var spoiled = tr.appendChild(document.createElement("td"));
        spoiled.appendChild(document.createTextNode(data[pro]["spoiled"]));

        var allowed = tr.appendChild(document.createElement("td"));
        allowed.appendChild(document.createTextNode(data[pro]["allowed"]));

        var attendance = tr.appendChild(document.createElement("td"));
        attendance.appendChild(document.createTextNode(data[pro]["attendance"].toFixed(2) + "%"));
    }
}


function candidates_display(region, region_number) {
    var data = JSON.parse(localStorage.getItem("candidates"));

    var candidates = document.getElementById("candidates");

    while (candidates.childElementCount > 1) {
        candidates.removeChild(candidates.lastChild);
    }


    if (localStorage.getItem("login") && region === 'con' && localStorage.getItem("appended") === "no") {
        var th = document.getElementById("candidates_table_header").appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Nowa wartość"));
        localStorage.setItem("appended", "yes");
    }

    var cand_count = 0;

    for (var cand in data) {
        cand_count += 1;

        var tr = candidates.appendChild(document.createElement("tr"));

        var position = tr.appendChild(document.createElement("td"));
        position.appendChild(document.createTextNode(cand_count));

        var name = tr.appendChild(document.createElement("td"));
        name.appendChild(document.createTextNode(data[cand]["name"]));
        name.id = "name_" + cand_count;

        var votes = tr.appendChild(document.createElement("td"));
        votes.appendChild(document.createTextNode(data[cand]["votes"]));

        var score = tr.appendChild(document.createElement("td"));
        score.appendChild(document.createTextNode(data[cand]["score"].toFixed(2) + "%"));

        if (localStorage.getItem("login") && region === 'con') {
            var new_votes = tr.appendChild(document.createElement("td"));
            new_votes.id = "cand_change_" + cand_count;

            var input = new_votes.appendChild(document.createElement("input"));
            input.type = "text";
            input.placeholder = data[cand]["votes"];
            input.id = "cand_" + cand_count;

            var button = new_votes.appendChild(document.createElement("button"));
            button.appendChild(document.createTextNode("Zmień"));
            var button_name = "cand_button_" + cand_count;
            button.id = button_name;
        }
    }

    if (localStorage.getItem("login") && region === 'con') {
        for (var i = 1; i <= cand_count; i++) {
            button = document.getElementById("cand_button_" + i);

            button.onclick = (function (index) {
                return function () {
                    var input = document.getElementById("cand_" + index);

                    if (isNaN(input.value) || !isFinite(input.value)) {
                        alert("Not numerical input!");

                    } else if (input.value < 0) {
                        alert("Negative value!");

                    } else {
                        change_request(region_number, index, input.value);
                        provinces_display(region, region_number);
                        candidates_display(region, region_number);
                    }
                }
            })(i);
        }
    }
}


function try_display_all(region, region_number) {
    if (localStorage.getItem("constituencies") === null
        || localStorage.getItem("voivodeships") === null
        || localStorage.getItem("counties") === null
        || localStorage.getItem("scores") === null)
        return;

    provinces_display(region, region_number);
    candidates_display(region, region_number);
}


function start_cand(cand, scores) {

    for (var j = 0; j < 12; j++) {
        var cand_obj = {};

        cand_obj["name"] = scores[j]["name"];
        cand_obj["votes"] = 0;

        cand.push(cand_obj);
    }
}


function count_votes(cand, scores) {
    for (var i = 0; i < scores.length / 12; i++) {
        for (var j = 0; j < 12; j++) {
            cand[j]["votes"] += scores[12 * i + j]["votes"];
        }
    }
}


function count_percent(cand, votes) {

    for (var i = 0; i < cand.length; i++) {
        if (votes === 0) {
            cand[i]["score"] = 100. / 12;

        } else {

            cand[i]["score"] = 100 * cand[i]["votes"] / votes;
        }
    }

    cand.sort(function(a,b) {return b["votes"] - a["votes"]});
}


function try_count(region, region_number) {

    if (localStorage.getItem("constituencies") === null
        || localStorage.getItem("voivodeships") === null
        || localStorage.getItem("counties") === null
        || localStorage.getItem("scores") === null)
        return;

    var all_cons = JSON.parse(localStorage.getItem("constituencies"));
    var all_cous = JSON.parse(localStorage.getItem("counties"));
    var all_vois = JSON.parse(localStorage.getItem("voivodeships"));
    var all_scores = JSON.parse(localStorage.getItem("scores"));

    var first_twelve = all_scores.slice(0, 12);

    var i;
    var j;
    var provinces = [];
    var cand = [];
    var scores = [];
    var cons = [];
    var cous = [];
    var begin = 0;
    var end = 0;
    var votes = 0;
    var new_votes = 0;
    var allowed = 0;
    var spoiled = 0;
    var con;
    var cou;
    var voi;

    start_cand(cand, first_twelve);

    switch(region) {
        case "con":

            con = all_cons[region_number];

            begin = 12 * region_number;

            scores = all_scores.slice(begin, begin + 12);

            count_votes(cand, scores);

            votes = 0;

            for (i = 0; i < cand.length; i++) {
                votes += cand[i]["votes"];
            }

            con["legit"] = votes;

            con["attendance"] = (con["allowed"] === 0 ? 0 : 100 * votes / con["allowed"]);

            provinces = [con];

            break;

        case "cou":

            cou = all_cous[region_number];

            cons = all_cons.slice(cou.lowest_con, cou.highest_con);

            votes = 0;

            for (i = 0; i < cons.length; i++) {

                begin = 12 * (parseInt(cou.lowest_con) + i);
                end = begin + 12;

                scores = all_scores.slice(begin, end);
                count_votes(cand, scores);

                new_votes = 0;

                for (j = 0; j < cand.length; j++) {
                    new_votes += cand[j]["votes"];
                }

                con = all_cons[begin / 12];

                con["legit"] = new_votes - votes;
                con["attendance"] = (con["allowed"] === 0 ? 0 : 100 * con["legit"] / con["allowed"]);

                otes = new_votes;

                provinces.push(con);
            }

            break;

        case "voi":

            voi = all_vois[region_number];

            cous = [];

            for (j = 0; j < all_cous.length; j++) {
                if (voi.lowest_con <= all_cous[j].lowest_con && all_cous[j].highest_con <= voi.highest_con) {
                    cous.push(all_cous[j]);
                }
            }

            votes = 0;

            for (i = 0; i < cous.length; i++) {
                cou = cous[i];

                begin = 12 * (parseInt(cou.lowest_con));
                end = 12 * (parseInt(cou.highest_con));

                scores = all_scores.slice(begin, end);
                count_votes(cand, scores);

                new_votes = 0;

                for (j = 0; j < cand.length; j++) {
                    new_votes += cand[j]["votes"];
                }

                cou["legit"] = new_votes - votes;

                votes = new_votes;

                allowed = 0;
                spoiled = 0;

                for (j = parseInt(cou.lowest_con); j < parseInt(cou.highest_con); j++) {
                    allowed += all_cons[j]["allowed"];
                    spoiled += all_cons[j]["spoiled"];
                }

                cou["allowed"] = allowed;
                cou["spoiled"] = spoiled;
                cou["attendance"] = (allowed === 0 ? 0 : 100 * cou["legit"] / allowed);

                provinces.push(cou);
            }

            break;

        case "pol":

            votes = 0;

            for (i = 0; i < all_vois.length; i++) {
                voi = all_vois[i];

                begin = 12 * (parseInt(voi.lowest_con));
                end = 12 * (parseInt(voi.highest_con));

                scores = all_scores.slice(begin, end);
                count_votes(cand, scores);

                new_votes = 0;

                for (j = 0; j < cand.length; j++) {
                    new_votes += cand[j]["votes"];
                }

                voi["legit"] = new_votes - votes;

                votes = new_votes;

                allowed = 0;
                spoiled = 0;

                for (j = parseInt(voi.lowest_con); j < parseInt(voi.highest_con); j++) {
                    allowed += all_cons[j]["allowed"];
                    spoiled += all_cons[j]["spoiled"];
                }

                voi["allowed"] = allowed;
                voi["spoiled"] = spoiled;
                voi["attendance"] = (allowed === 0 ? 0 : 100 * voi["legit"] / allowed);

                voi["name"] = voi["name"].toLowerCase();

                provinces.push(voi);
            }

            break;
    }

    count_percent(cand, votes);

    localStorage.setItem("candidates", JSON.stringify(cand));

    localStorage.setItem("provinces", JSON.stringify(provinces));

}


function set_search(constituencies) {
    var con_list = JSON.parse(constituencies);

    var con_names = [];

    for (var i = 0; i < con_list.length; i++) {
        con_names.push(con_list[i]["name"]);
    }

    $('#search_name').autocomplete({
        source: con_names.sort()
    });

    var button = document.getElementById("search_button");
    button.onclick = function() {
        var field = document.getElementById("search_name");


        for (var i = 0; i < con_list.length; i++) {
            if (con_list[i]["name"] === field.value) {
                location.href = '/con' + con_list[i]["id"];
            }
        }
    };

}

function db_request(region) {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/" + region + "/");
    req.addEventListener("error", function() {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function() {

        localStorage.setItem(region, this.responseText);

        if (region === "constituencies")
            set_search(this.responseText);

        try_count(localStorage.getItem("region"), localStorage.getItem("region_number"));

        try_display_all(localStorage.getItem("region"), localStorage.getItem("region_number"));
    });

    req.send();
}


function init() {
    localStorage.setItem("appended", "no");

    var path = window.location.pathname;
    var region = "pol";
    var region_number = 0;

    if (path !== "/") {
        region = path.slice(1, 4);
        region_number = path.slice(4);
    }

    localStorage.setItem("region", region);
    localStorage.setItem("region_number", region_number);

    login_display();

    region_display(region, region_number);

    localStorage.removeItem("provinces");
    localStorage.removeItem("candidates");


    if (localStorage.getItem("constituencies") === null) {
        db_request("constituencies");
    } else {
        set_search(localStorage.getItem("constituencies"));
    }

    if (localStorage.getItem("voivodeships") === null) {
        db_request("voivodeships");
    }

    if (localStorage.getItem("counties") === null) {
        db_request("counties");
    }

    if (localStorage.getItem("scores") === null) {
        db_request("scores");
    }

    try_count(region, region_number);
    try_display_all(region, region_number);
}