function auth_request(login, password) {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/authenticate/" + login + "/" + password);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {

        if (this.responseText == "1") {
            alert("Successfully logged in (reload page if login not displayed)");

            localStorage.setItem("login", login);
            localStorage.setItem("password", password);

        } else {
            alert("Failed to login");

            localStorage.removeItem("login");
            localStorage.removeItem("password");
        }
    });

    req.send();
}


function register_request(login, password) {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/register/" + login + "/" + password);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {
        
        if (this.responseText == "1") {
            alert("Successfully registered");

        } else {
            alert("Failed to register");
        }
    });

    req.send();
}


function login_display() {
    var div = document.getElementById("login");

    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }

    var login = localStorage.getItem("login");
    if (login != null) {

        var welcome_div = div.appendChild(document.createElement("div"));
        welcome_div.appendChild(document.createTextNode("Cześć, " + login + "!"));

        var logout_div = div.appendChild(document.createElement("div"));
        var a = logout_div.appendChild(document.createElement("a"));
        a.appendChild(document.createTextNode("Wyloguj"));
        a.onclick = function() {
            localStorage.removeItem("login");
            localStorage.removeItem("password");

            if (localStorage.getItem("appended") == "yes") {
                localStorage.setItem("appended", "no");
                var header = document.getElementById("candidates_table_header");
                header.removeChild(header.lastChild);

                var cand_count = 12;

                for (var i = 1; i <= cand_count; i++) {
                    var td = document.getElementById("cand_change_" + i);
                    td.parentNode.removeChild(td);
                }

            }


            login_display();
        }

    } else {
        var login_div = div.appendChild(document.createElement("div"));

        var login_input = login_div.appendChild(document.createElement("input"));
        login_input.type = "text";login_input.placeholder = "Login";

        var password_input = login_div.appendChild(document.createElement("input"));
        password_input.type = "password";
        password_input.placeholder = "Hasło";

        var submit = login_div.appendChild(document.createElement("button"));
        submit.appendChild(document.createTextNode("Login"));
        submit.onclick = function() {
            auth_request(login_input.value, password_input.value);

            login_display();
        };

        var register_div = div.appendChild(document.createElement("div"));

        var register_login = register_div.appendChild(document.createElement("input"));
        register_login.type = "text";
        register_login.placeholder = "Login";

        var register_password = register_div.appendChild(document.createElement("input"));
        register_password.type = "password";
        register_password.placeholder = "Hasło";

        var register = register_div.appendChild(document.createElement("button"));
        register.appendChild(document.createTextNode("Rejestracja"));
        register.onclick = function() {
            register_request(register_login.value, register_password.value);
        }
    }
}

function provinces_display(provinces_list, region) {
    var data = JSON.parse(provinces_list);

    var provinces = document.getElementById("provinces");

    while (provinces.childElementCount > 1) {
        provinces.removeChild(provinces.lastChild);
    }


    for (var pro in data) {
        var tr = provinces.appendChild(document.createElement("tr"));

        var name = tr.appendChild(document.createElement("td"));
        var link = name.appendChild(document.createElement("a"));
        link.appendChild(document.createTextNode(data[pro]["name"]));

        link.href = window.location.pathname;

        switch(region) {
            case "pol":
                link.href = "/woj" + data[pro]["id"];
                break;
            case "woj":
                link.href = "/cou" + data[pro]["id"];
                break;
            case "cou":
                link.href = "/con" + data[pro]["id"];
                break;
        }

        var legit = tr.appendChild(document.createElement("td"));
        legit.appendChild(document.createTextNode(data[pro]["legit_votes"]));

        var spoiled = tr.appendChild(document.createElement("td"));
        spoiled.appendChild(document.createTextNode(data[pro]["spoiled_votes"]));

        var allowed = tr.appendChild(document.createElement("td"));
        allowed.appendChild(document.createTextNode(data[pro]["allowed"]));

        var attendance = tr.appendChild(document.createElement("td"));
        attendance.appendChild(document.createTextNode(data[pro]["attendance"].toFixed(2) + "%"));
    }
}

function provinces_request(region, region_number) {
    var req = new XMLHttpRequest();
    switch(region) {
        case "pol":
            req.open("GET", "http://localhost:8000/poland/");
            break;
        case "woj":
            req.open("GET", "http://localhost:8000/voivodeship/" + region_number);
            break;
        case "cou":
            req.open("GET", "http://localhost:8000/county/" + region_number);
            break;
        case "con":
            req.open("GET", "http://localhost:8000/constituency/" + region_number);
            break;
    }
    req.addEventListener("error", function() {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function() {
        provinces_display(this.responseText, region);
        localStorage.setItem("provinces", this.responseText);
    });

    req.send();
}


function change_request(region_number, index, value) {
    var login = localStorage.getItem("login");
    var password = localStorage.getItem("password");
    if (login === null || password === null)
        return;
    var req_auth = new XMLHttpRequest();
    var logged = 1;
    req_auth.open("GET", "http://localhost:8000/authenticate/" + login + "/" + password);
    req_auth.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req_auth.addEventListener("load", function () {

        if (this.responseText != "1") {
            logged = 0;
        }
    });

    if (logged === 0)
        return;

    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/change/" + region_number + "/" + index + "/" + value);
    req.addEventListener("error", function () {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function () {

        if (this.responseText == "1") {
            alert("Successfully changed, please reload page to see changes");

        } else {
            alert("Failed to change");
        }
    });

    req.send();
}



function candidates_display(candidates_list, region, region_number) {
    var data = JSON.parse(candidates_list);

    var candidates = document.getElementById("candidates");

    while (candidates.childElementCount > 1) {
        candidates.removeChild(candidates.lastChild);
    }


    if (localStorage.getItem("login") && region === 'con' && localStorage.getItem("appended") == "no") {
        var th = document.getElementById("candidates_table_header").appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Nowa wartość"));
        localStorage.setItem("appended", "yes");
    }

    var cand_count = 0;

    for (var cand in data) {
        cand_count += 1;

        var tr = candidates.appendChild(document.createElement("tr"));

        var position = tr.appendChild(document.createElement("td"));
        position.appendChild(document.createTextNode(data[cand]["position"]));

        var name = tr.appendChild(document.createElement("td"));
        name.appendChild(document.createTextNode(data[cand]["name"]));
        name.id = "name_" + data[cand]["position"];

        var votes = tr.appendChild(document.createElement("td"));
        votes.appendChild(document.createTextNode(data[cand]["votes_count"]));

        var score = tr.appendChild(document.createElement("td"));
        score.appendChild(document.createTextNode(data[cand]["votes_percentage"].toFixed(2) + "%"));

        if (localStorage.getItem("login") && region === 'con') {
            var new_votes = tr.appendChild(document.createElement("td"));
            new_votes.id = "cand_change_" + data[cand]["position"];

            var input = new_votes.appendChild(document.createElement("input"));
            input.type = "text";
            input.placeholder = data[cand]["votes_count"];
            input.id = "cand_" + data[cand]["position"];

            var button = new_votes.appendChild(document.createElement("button"));
            button.appendChild(document.createTextNode("Zmień"));
            var button_name = "cand_button_" + data[cand]["position"];
            button.id = button_name;
        }
    }

    if (localStorage.getItem("login") && region === 'con') {
        for (var i = 1; i <= cand_count; i++) {
            button = document.getElementById("cand_button_" + i);

            button.onclick = (function (index) {
                return function () {
                    var input = document.getElementById("cand_" + index);

                    if (isNaN(input.value) || !isFinite(input.value)) {
                        alert("Not numerical input!");

                    } else if (input.value < 0) {
                        alert("Negative value!");

                    } else {
                        change_request(region_number, index, input.value);
                        candidates_display(candidates_list, region, region_number);
                    }
                }
            })(i);
        }
    }
}


function candidates_request(region, region_number) {
    var req = new XMLHttpRequest();
    switch(region) {
        case "pol":
            req.open("GET", "http://localhost:8000/poland_cand/");
            break;
        case "woj":
            req.open("GET", "http://localhost:8000/voivodeship_cand/" + region_number);
            break;
        case "cou":
            req.open("GET", "http://localhost:8000/county_cand/" + region_number);
            break;
        case "con":
            req.open("GET", "http://localhost:8000/constituency_cand/" + region_number);
            break;
    }
    req.addEventListener("error", function() {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function() {
        candidates_display(this.responseText, region, region_number);
        localStorage.setItem("candidates", this.responseText);
    });

    req.send();
}


function constituencies_request() {
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8000/constituencies/");
    req.addEventListener("error", function() {
        alert("Error: " + this.responseText);
    });
    req.addEventListener("load", function() {

        localStorage.setItem("constituencies", this.responseText);
    });

    req.send();
}


function region_display(region, region_number) {
    switch(region) {
        case "pol":
            document.getElementById("provinces_header").appendChild(document.createTextNode("województwach"));
            document.getElementById("candidates_header").appendChild(document.createTextNode("kraju"));
            break;
        case "woj":
            document.getElementById("provinces_header").appendChild(document.createTextNode("okręgach"));
            document.getElementById("candidates_header").appendChild(document.createTextNode("województwa"));
            break;
        case "cou":
            document.getElementById("provinces_header").appendChild(document.createTextNode("gminach"));
            document.getElementById("candidates_header").appendChild(document.createTextNode("okręgu"));
            break;
        case "con":
            document.getElementById("provinces_header").appendChild(document.createTextNode("gminie"));
            document.getElementById("candidates_header").appendChild(document.createTextNode("gminy"));
            break;
    }
}


function init() {
    localStorage.setItem("appended", "no");

    var path = window.location.pathname;
    var region = "pol";
    var region_number = 0;

    if (path != "/") {
        region = path.slice(1, 4);
        region_number = path.slice(4);
    }

    var prev_region = localStorage.getItem("region");
    var prev_number = localStorage.getItem("region_number");

    if (prev_region != null && prev_number != null) {
        if (prev_region == region && prev_number == region_number) {

            var provinces = localStorage.getItem("provinces");
            if (provinces != null) {
                provinces_display(provinces, region);
            }

            var candidates = localStorage.getItem("candidates");
            if (candidates != null) {
                candidates_display(candidates, region, region_number);
            }
        }
    }

    login_display();

    region_display(region, region_number);

    localStorage.setItem("region", region);
    localStorage.setItem("region_number", region_number);
    
    if (localStorage.getItem("constituencies") == null) {
        constituencies_request();
    }

    var cons_list = JSON.parse(localStorage.getItem("constituencies"));

    var constituencies = [];

    var cons_length;

    if (cons_list != null)
        cons_length = cons_list.length;
    else
        cons_length = 0;

    for (var i = 0; i < cons_length; i++) {
        constituencies.push(cons_list[i]["name"]);
    }

    $('#search_name').autocomplete({
        source: constituencies.sort()
    });

    var button = document.getElementById("search_button");
    button.onclick = function() {
        var field = document.getElementById("search_name");

        for (var i = 0; i < cons_length; i++) {
            if (cons_list[i]["name"] == field.value) {
                location.href = '/con' + cons_list[i]["id"];
            }
        }
    };

    provinces_request(region, region_number);

    candidates_request(region, region_number);
}