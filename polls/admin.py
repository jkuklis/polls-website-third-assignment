from django.contrib import admin

from .models import Score, Constituency, County, District, Voivodeship
# Register your models here.

admin.site.register(Score)
admin.site.register(Constituency)
admin.site.register(County)
admin.site.register(District)
admin.site.register(Voivodeship)