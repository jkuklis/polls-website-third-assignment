import csv
from polls.models import Voivodeship, County, District, Constituency, Score

districts = []

with open('./polls/static/pkw2000.csv', 'rt', encoding='utf8') as csvfile:
    reader = csv.DictReader(csvfile)
    poll_fields = reader.fieldnames[5:23]
    candidates = reader.fieldnames[11:23]

    i = 0
    print(i)

    voivodeship = None
    county = None
    district = None

    for row in reader:
        i += 1
        print(i)

        if i == 1:
            voivodeship = Voivodeship.objects.create(name=row['Województwo'], lowest_cons_number=0)

        if voivodeship.name != row['Województwo']:
            voivodeship.highest_cons_number = i - 1
            voivodeship.save()
            voivodeship = Voivodeship.objects.create(name=row['Województwo'], lowest_cons_number=i-1)

        if i == 1:
            county = County.objects.create(
                number=row['Nr okręgu'],
                lowest_cons_number=0,
            )

        if county.number != row['Nr okręgu']:
            county.highest_cons_number = i - 1
            county.save()
            county = County.objects.create(
                number=row['Nr okręgu'],
                lowest_cons_number=i-1,
            )

        if row['Powiat'] not in districts:
            districts.append(row['Powiat'])
            district = District.objects.create(
                name=row['Powiat'],
            )
        else:
            district = District.objects.get(name=row['Powiat'])

        constituency = Constituency.objects.create(
            number=row['Kod gminy'],
            name=row['Gmina'],
            allowed=row['Uprawnieni'],
            ballots=row['Karty wydane'],
            votes=row['Głosy oddane'],
            spoiled_votes=row['Głosy nieważne'],
            district=district,
        )

        for candidate in candidates:
            Score.objects.create(
                name=candidate,
                constituency=constituency,
                votes_count=row[candidate],
            )

    voivodeship.highest_cons_number = i
    voivodeship.save()

    county.highest_cons_number = i
    county.save()
